﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_helloworld = New System.Windows.Forms.Button()
        Me.lbl_instruct = New System.Windows.Forms.Label()
        Me.lbl_affich = New System.Windows.Forms.Label()
        Me.btn_helloben = New System.Windows.Forms.Button()
        Me.btn_hellosmo = New System.Windows.Forms.Button()
        Me.txt_name = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btn_helloworld
        '
        Me.btn_helloworld.Location = New System.Drawing.Point(24, 52)
        Me.btn_helloworld.Name = "btn_helloworld"
        Me.btn_helloworld.Size = New System.Drawing.Size(141, 30)
        Me.btn_helloworld.TabIndex = 0
        Me.btn_helloworld.Text = "Say Hello World"
        Me.btn_helloworld.UseVisualStyleBackColor = True
        '
        'lbl_instruct
        '
        Me.lbl_instruct.AutoSize = True
        Me.lbl_instruct.Location = New System.Drawing.Point(20, 179)
        Me.lbl_instruct.Name = "lbl_instruct"
        Me.lbl_instruct.Size = New System.Drawing.Size(157, 20)
        Me.lbl_instruct.TabIndex = 1
        Me.lbl_instruct.Text = "We are going to say :"
        '
        'lbl_affich
        '
        Me.lbl_affich.AutoSize = True
        Me.lbl_affich.Location = New System.Drawing.Point(211, 179)
        Me.lbl_affich.Name = "lbl_affich"
        Me.lbl_affich.Size = New System.Drawing.Size(13, 20)
        Me.lbl_affich.TabIndex = 2
        Me.lbl_affich.Text = " "
        '
        'btn_helloben
        '
        Me.btn_helloben.Location = New System.Drawing.Point(24, 297)
        Me.btn_helloben.Name = "btn_helloben"
        Me.btn_helloben.Size = New System.Drawing.Size(124, 31)
        Me.btn_helloben.TabIndex = 3
        Me.btn_helloben.Text = "Hello Ben"
        Me.btn_helloben.UseVisualStyleBackColor = True
        '
        'btn_hellosmo
        '
        Me.btn_hellosmo.Location = New System.Drawing.Point(215, 297)
        Me.btn_hellosmo.Name = "btn_hellosmo"
        Me.btn_hellosmo.Size = New System.Drawing.Size(109, 31)
        Me.btn_hellosmo.TabIndex = 4
        Me.btn_hellosmo.Text = "Hello who ?"
        Me.btn_hellosmo.UseVisualStyleBackColor = True
        '
        'txt_name
        '
        Me.txt_name.Location = New System.Drawing.Point(215, 251)
        Me.txt_name.Name = "txt_name"
        Me.txt_name.Size = New System.Drawing.Size(109, 26)
        Me.txt_name.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(440, 407)
        Me.Controls.Add(Me.txt_name)
        Me.Controls.Add(Me.btn_hellosmo)
        Me.Controls.Add(Me.btn_helloben)
        Me.Controls.Add(Me.lbl_affich)
        Me.Controls.Add(Me.lbl_instruct)
        Me.Controls.Add(Me.btn_helloworld)
        Me.Name = "Form1"
        Me.Text = "Hello World"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_helloworld As Button
    Friend WithEvents lbl_instruct As Label
    Friend WithEvents lbl_affich As Label
    Friend WithEvents btn_helloben As Button
    Friend WithEvents btn_hellosmo As Button
    Friend WithEvents txt_name As TextBox
End Class
