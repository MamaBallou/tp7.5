﻿Imports HelloWorldLib
Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btn_helloworld.Click
        MessageBox.Show(Greetings.HelloWorld())
    End Sub

    Private Sub btn_helloben_Click(sender As Object, e As EventArgs) Handles btn_helloben.Click
        lbl_affich.Text = Greetings.SayHello("Ben")
    End Sub

    Private Sub btn_hellosmo_Click(sender As Object, e As EventArgs) Handles btn_hellosmo.Click
        lbl_affich.Text = Greetings.SayHello(txt_name.Text)
    End Sub
End Class
